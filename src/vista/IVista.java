
package vista;

/*
 * Fichero: IVista.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 10-dic-2015
 */
public interface IVista<T> {

    public T obtener();

    public void mostrar(T t);
    }
