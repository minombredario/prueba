/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class VistaMenu {
    Scanner opcion = new Scanner(System.in);
    String MenuPrincipal;
    VistaTerminal vt;
    
    public char MenuEstructura(){
        
        Singleton titulo = Singleton.getSingleton("Consulta Médica Privada\n");
        System.out.println(titulo.getTitulo());
        
        char opcion = ' ';
        
        System.out.println("******************************");
        System.out.println("**      MENU ESTRUCTURA     **");
        System.out.println("******************************");
        System.out.println("**  a. ARRAY:               **");
        System.out.println("**  h. HASHSET:             **");
        System.out.println("**  f. FICHERO:             **");
        System.out.println("**  s. Salir.               **");
        System.out.print("    Opcion: ");
        
        opcion = vt.leerLetra();
        return opcion;
    }
    public char MenuPrincipal(){
        
        System.out.println("******************************");
        System.out.println("**           MENU           **");
        System.out.println("******************************");
        System.out.println("**  p. Gestionar Paciente:  **");
        System.out.println("**  m. Gestionar Medico:    **");
        System.out.println("**  c. Gestionar Cita:      **");
        System.out.println("**  s. Salir.               **");
        System.out.print("    Opcion: ");

        char opcion = vt.leerLetra();
        return opcion;
    }

    public void MenuMedico(){
        
        System.out.println("******************************");
        System.out.println("**       MENU  MEDICO       **");
        System.out.println("******************************");
    }

    public void MenuPaciente(){
        
        System.out.println("******************************");
        System.out.println("**       MENU  PACIENTE     **");
        System.out.println("******************************");
    }

    public void MenuCita(){
        
        System.out.println("******************************");
        System.out.println("**       MENU  CITA         **");
        System.out.println("******************************");
    }
    
    public char MenuCrud(){
        
        System.out.println("**  c. CREATE               **");
        System.out.println("**  r. READ                 **");
        System.out.println("**  u. UPDATE               **");
        System.out.println("**  d. DELETE               **");
        System.out.println("**  s. SALIR                **");
        System.out.print("    Opcion: ");
        
       char opcion = ' ';
       return opcion;
    }
}