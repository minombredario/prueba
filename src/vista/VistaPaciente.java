/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.IModelo;
import modelo.Paciente;
import modelo.Persona;

/**
 * Fichero: VistaPaciente.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaPaciente implements IVista<Paciente> {

    Paciente paciente = new Paciente();
    Persona persona = new Persona();
    VistaTerminal vt = new VistaTerminal();
    VistaPersona vistapersona = new VistaPersona();
    
   
    public Paciente obtener() {
        String nss;

        System.out.println("**   ALTA PACIENTE   **");
        
        System.out.print("Número Seguridad Social: ");
        nss = vt.pedirString();
        paciente.setNss(nss);
        
        persona = vistapersona.obtener();
        
        paciente.setId(persona.getId());
        paciente.setNombre(persona.getNombre());
        paciente.setEdad(persona.getEdad());
        paciente.setTelefono(persona.getTelefono());
        paciente.setObservaciones(persona.getObservaciones());

        return paciente;
    }

    public void mostrar(Paciente paciente) {
        System.out.println("* DATOS DEL PACIENTE");
        System.out.println("* ------------------------------------------------------------------------------");
        System.out.println("* Id: " + paciente.getId());
        System.out.println("* Nombre: " + paciente.getNombre() + "\tNSS: "
           + paciente.getNss());
        System.out.println("* Edad: " + paciente.getEdad() + " años"
           + "\tTelefono: " + paciente.getTelefono());
        System.out.println("* Observaciones: " + paciente.getObservaciones());
        //System.out.println(paciente);
    }

    public void mostrarPacientes(HashSet hashset) {
        IVista<Paciente> vp = new VistaPaciente();
        Iterator iterator = hashset.iterator();
        Paciente paciente = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            paciente = (Paciente) iterator.next();
            vp.mostrar(paciente);
            System.out.println("********************************************************************************\n");
        }
    }
  
}