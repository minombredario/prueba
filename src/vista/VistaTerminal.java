package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: VistaTerminal.java
 * 
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 21-oct-2015
 */
public class VistaTerminal {

    public int pedirInt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea;
        int numero = 0;
        try{
             linea = buffer.readLine();
            numero = Integer.parseInt(linea);
        }catch (Exception e){
            System.err.println("Introduce un valor númerico");
            
        }
        return numero;

    }

    public String pedirString() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea = null;

        try {
            linea = buffer.readLine();
        } catch (Exception e) {
            System.err.println("Introduce texto");
        }
        return linea;
    }

    public void Esperar() throws IOException {
        Scanner continuar = new Scanner(System.in);
        System.out.println("Pulsa cualquier tecla para continuar");
        continuar.nextLine();
    }
           
    //Función para mostrar un texto por pantalla
    public void mostrarTexto(String texto) {
        System.out.print(texto);
    }
    
    //Función para validar un telefono 
    public boolean eTelefono (String telefono){
        final String patronTelefono = "[^[9|8|7|6]\\d{8}$";
    Pattern patron = Pattern.compile(patronTelefono);
    Matcher compara = patron.matcher(telefono);
    return compara.matches();
    }
   
    //Función para leer una letra por teclado y pasarla a minúscula
    public static char leerLetra() {
        String linea;
        char opcion;
        Scanner sc = new Scanner (System.in);
        linea = sc.next().toLowerCase();
        opcion = linea.charAt(0);
        return opcion;
    }
}
