/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Medico;
import modelo.Persona;
import vista.VistaTerminal;

/**
 * Fichero: VistaMedico.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaMedico implements IVista<Medico> {

    Medico medico = new Medico();
    Persona persona = new Persona();
    VistaPersona vp = new VistaPersona();
    VistaTerminal vt = new VistaTerminal();

    @Override
    public Medico obtener (){
        String ncolegiado;
        String especialidad;

        System.out.println("**   ALTA MEDICO   **");
        
        System.out.print("Número de colegiado: ");
        ncolegiado = vt.pedirString();
        
        System.out.print("Especialidad: ");
        especialidad = vt.pedirString();
        
        persona = vp.obtener();
        
        medico.setEspecialidad(especialidad);
        medico.setNcolegiado(ncolegiado);
        medico.setId(persona.getId());
        medico.setNombre(persona.getNombre());
        medico.setEdad(persona.getEdad());
        medico.setTelefono(persona.getTelefono());
        medico.setObservaciones(persona.getObservaciones());

        return medico;
    }

    public void mostrar(Medico medico) {
        
        System.out.println("* DATOS DEL MEDICO");
        System.out.println("* ------------------------------------------------------------------------------");
        System.out.println("* Id: " + medico.getId());
        System.out.println("* Nombre: " + medico.getNombre() + "\tNº Colegiado: "
                           + medico.getNcolegiado());
        System.out.println("* Especialidad: " + medico.getEspecialidad());
        System.out.println("* Edad: " + medico.getEdad()+ " años" 
                           + "\tTelefono: " + medico.getTelefono());
        System.out.println("* Observaciones: " + medico.getObservaciones());
        //System.out.println(medico);

    }
    
     public void mostrarMedicos(HashSet hashset){
        VistaMedico vistamedico = new VistaMedico();
        Iterator iterator = hashset.iterator();
        Medico medico = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            medico = (Medico) iterator.next();
            vistamedico.mostrar(medico);
            System.out.println("********************************************************************************");
        }
    }

}
