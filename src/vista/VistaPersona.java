package vista;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Persona;

/**
 * Fichero: VistaPersona.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 14-dic-2015
 */
public class VistaPersona implements IVista<Persona>{
    private Object vt;
    Persona persona = new Persona();

    public Persona obtener() {
        String id;
        String nombre = "";
        int telefono = 123456789;
        String observaciones = "";
        int edad = 0;
        boolean IdError = true, EError = true, TError = true;

        VistaTerminal vt = new VistaTerminal();
    
       
        //System.out.println("ID: " + persona.getId());
        //id = persona.getId();
        System.out.print("ID: ");
        id = vt.pedirString();
        
        
        System.out.print("Nombre: ");
        nombre = vt.pedirString();
        
    while (edad <= 0){
        do {
            try {
                
                System.out.print("Edad: ");
                edad = (int) vt.pedirInt();
                EError = false;

            } catch (NumberFormatException e) {
                System.out.println("Error: Introduce un valor númerico");

            } catch (IOException ex) {
                Logger.getLogger(VistaPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (edad <=0){ System.out.println("La edad ha de ser positiva");}
        } while (EError);
    }

        do {
            try {
                System.out.print("Telefono: ");
                telefono = vt.pedirInt();
                TError = false;
            } catch (NumberFormatException e) {
                System.out.println("Error: Introduce un valor númerico");
            } catch (IOException ex) {
                Logger.getLogger(VistaPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (TError);

        System.out.print("Observaciones: ");
        observaciones = vt.pedirString();
        
        persona.setId(id);
        persona.setNombre(nombre);
        persona.setEdad(edad);
        persona.setTelefono(telefono);
        persona.setObservaciones(observaciones);

        return persona;
     
    }

    public void mostrar(Persona persona) {
        int id;
        System.out.println("Id: " + persona.getId());

    }
    
    public void mostrartodos(HashSet hashset){
        VistaPersona vistapersona = new VistaPersona();
        Iterator iterator = hashset.iterator();
        Persona persona = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            persona = (Persona) iterator.next();
            vistapersona.mostrar(persona);
            System.out.println("********************************************************************************");
        }
    }
}
