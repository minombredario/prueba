/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

/*
 * Fichero: main.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 8-dic-2015
 */
public class Singleton {

    private String titulo;
    private static Singleton misingleton;

    private Singleton(String t) {
        titulo = t;
    }

    public static Singleton getSingleton(String t) {

        if (misingleton == null) {
            misingleton = new Singleton(t);
            System.out.println("Creado objeto");
            return misingleton;

        } else {
            System.out.println("Error objeto no creado");
        }

        return misingleton;

    }

    String getTitulo() {
        return titulo;
    }

}
