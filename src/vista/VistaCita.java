
package vista;

import controlador.ControladorPaciente;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Cita;
import modelo.IModelo;
import modelo.Medico;
import modelo.Paciente;

/**
 * Fichero: VistaCita.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 20-oct-2015
 */
public class VistaCita implements IVista<Cita> {

    IModelo modelo;
     
    private String id;
    private String hora;
    private String dia;
    private String idpaciente, nombrepaciente;
    ControladorPaciente cont;
   
    @Override
    public Cita obtener() {
        
        VistaTerminal vt = new VistaTerminal();
        Cita cita = new Cita();
       
        System.out.print("Id: ");
        id = vt.pedirString();
        
        vt.mostrarTexto("Id del paciente: ");
        idpaciente = vt.pedirString();
        cont.buscarPaciente(idpaciente);
        
                
        System.out.print("Hora: ");
        hora = vt.pedirString();
        
        System.out.print("Dia: ");
        dia = vt.pedirString();
        
        cita.setId(id);
        cita.setHora(hora);
        cita.setDia(dia);
        cita.setPaciente(nombrepaciente);
        //cita.setPaciente(ControladorCita.nombrepaciente);
        //cita.setMedico(ControladorCita.nombremedico);
        return cita;
    }

    @Override
    public void mostrar(Cita cita) {
        
        System.out.println("**************************");
        System.out.println("**   DATOS DE LA CITA   **");
        System.out.println("**************************");
        
        System.out.println("ID: " + cita.getId() + "\nPaciente:" + cita.getPaciente()
                           + "\tMedico: " + cita.getMedico());
        System.out.println("Dia: " + cita.getDia() + "\t\tHora: " +
                           cita.getHora());

    }
    
    public void mostrarCitas(HashSet hashset){
        VistaCita vistacita = new VistaCita();
        Iterator iterator = hashset.iterator();
        Cita cita = null;

        while (iterator.hasNext()) {
            System.out.println("\n*******************************************"
                    + "*************************************");
            cita = (Cita) iterator.next();
            vistacita.mostrar(cita);
            System.out.println("***********************************************"
                    + "*********************************");
        }
    }

   }
