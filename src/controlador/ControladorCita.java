package controlador;

import java.io.IOException;
import vista.VistaMenu;
import vista.VistaTerminal;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Cita;
import modelo.IModelo;
import modelo.Medico;
import modelo.Paciente;
import vista.IVista;
import vista.VistaCita;


/**
 * Fichero: ControladorCita.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public class ControladorCita {
    //public static String nombrepaciente;
    //public static String nombremedico;
   
    VistaTerminal vt = new VistaTerminal();
    VistaMenu menu = new VistaMenu();
    
    IModelo modelo;
    VistaCita vistacita;
    Cita cita;
        
    public ControladorCita (VistaCita vistacita , IModelo modelo) throws IOException{
        this.vistacita = vistacita;
        this.modelo = modelo;
        MenuCita();
    }
    public char MenuCita() throws IOException{
        boolean test = false;
        char opcion = ' ';
        do {
            menu.MenuCita();
            menu.MenuCrud();
            opcion = vt.leerLetra();

            switch (opcion) {
                case 'c': // Crea cita
                    create();
                    test = true;
                    break;
                case 'r':
                    read();
                    test = true;
                    break;
                case 'u':
                    update();
                    test = true;
                    break;
                case 'd':
                    delete();
                    test = true;
                    break;
                case 's':
                    vt.mostrarTexto("\nvolviendo al menu Estructura...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                }
               
        } while (test);
        return opcion;

    }
    
    public void create(){
        String idmedico, idpaciente;
        
        Medico medico = null;
        boolean em = true;
        
        vt.mostrarTexto("\n\n**   ALTA CITA   **\n");
        
        /*vt.mostrarTexto("Id del paciente: ");
        idpaciente = vt.pedirString();
        buscarPaciente(idpaciente);*/
        
        
        /*do{
            vt.mostrarTexto("Id del Medico: ");
            idmedico = vt.pedirString();
            Iterator iterator2 = modelo.readm().iterator();
            while (iterator2.hasNext()) {
                Medico me = (Medico) iterator2.next();
                if (idmedico.equals(me.getId())) {
                    nombremedico = me.getNombre();
                    em = false;
                } else {vt.mostrarTexto("Ningun medico asociado\n");}
            }
            if (medico != null) {
              }
        }while (em);*/
        cita = vistacita.obtener();
        modelo.create(cita);
        
    }
    
    public void read(){
        
        HashSet hashset = modelo.readc();
        vistacita.mostrarCitas(hashset);
        

    }

    public void update(){
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Cita cita = null;
        Iterator iterator = modelo.readc().iterator();
        while (iterator.hasNext()) {
            Cita ci = (Cita) iterator.next();
            if (id.equals(ci.getId())) {
                cita = ci;
            }
            if (cita != null){
                IVista<Cita> vp = new VistaCita();
            cita = vp.obtener();
            cita.setId(id);
            modelo.update(cita);
            vt.mostrarTexto("Datos actualizados\n");
            break;
            } else {vt.mostrarTexto("La cita no existe\n");}
        }
    }

    public void delete(){
        
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Cita cita = null;
        Iterator iterator = modelo.readc().iterator();
        while (iterator.hasNext()) {
            Cita ci = (Cita) iterator.next();
            if (id.equals(ci.getId())) {
                cita = ci;
            }
            if (cita != null) {
            modelo.delete(cita);
            vt.mostrarTexto("Cita borrada\n");
            break;
            } else {vt.mostrarTexto("La cita no existe\n");}
        }
    }
    
    public void buscarPaciente(String idpaciente){
        String nombrepaciente;
        Paciente paciente = null;
        boolean ep = true;
        
        do{
            Iterator iterator = modelo.readp().iterator();
            while (iterator.hasNext()) {
                 Paciente pa = (Paciente) iterator.next();
                if (idpaciente.equals(pa.getId())) {
                    nombrepaciente =pa.getNombre();
                    ep = false;
                } else {vt.mostrarTexto("Ningun paciente asociado\n");}
            }
        }while (ep);
        
    }
    
}