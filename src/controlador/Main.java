/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import modelo.IModelo;
import vista.VistaMenu;

/**
 * Fichero: main.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 14-dic-2015
 */
public class Main {

    public static void main(String[] args) throws IOException {

        IModelo modelo = null;

        VistaMenu vista = new VistaMenu();

        Controlador menu = new Controlador(modelo, vista);
    }

}
