package controlador;

import java.io.IOException;
import vista.VistaMenu;
import vista.VistaTerminal;
import modelo.IModelo;
import modelo.ModeloArray;
import modelo.ModeloFichero;
import modelo.ModeloHashSet;
import vista.VistaCita;
import vista.VistaMedico;
import vista.VistaPaciente;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class Controlador {

    VistaTerminal vt = new VistaTerminal();
    VistaMenu menu;
    IModelo modelo;
    private char opcion;
    
    ControladorMedico cme;
    ControladorPaciente cpa;
    
    public Controlador(IModelo modelo, VistaMenu menu) throws IOException {
        this.menu = menu;
        this.modelo = modelo;
        opcion = menuEstructura();
        
        
        //inicio();
    }
    
    public char menuEstructura() throws IOException{
       
        char opcion = ' ';
        boolean test = true;
       
        do {
            
            opcion = menu.MenuEstructura();
           
            switch (opcion) {
                case 'a':
                    // Si es array
                    modelo = new ModeloArray();
                    //test = true;
                    inicio();
                    break;
                case 'h':
                    // Si es hashset
                    modelo = new ModeloHashSet();
                    //test = true;
                    inicio();
                    break;
                case 'f':
                    modelo = new ModeloFichero();
                    //test = true;
                    inicio();
                   break;
                case 's':
                    vt.mostrarTexto("\nHasta luego...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                }
               
        } while (test == true);
        return opcion;
    }

    public char inicio() throws IOException {

        char opcion = ' ';
        boolean test = true;
        do {
            
            opcion = menu.MenuPrincipal();

            switch (opcion) {
                case 'p':
                    paciente();
                    break;
                case 'm':
                    medico();
                    break;
                case 'c':
                    cita();
                    break;
                case 's':
                    vt.mostrarTexto("\nHasta luego...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                    
                }
        } while (test == true);
        return opcion;
    }

    private void paciente() throws IOException {
        VistaPaciente vistapaciente = new VistaPaciente();
        ControladorPaciente cp = new ControladorPaciente(vistapaciente, modelo);
    }

    private void medico() throws IOException {
        VistaMedico vistamedico = new VistaMedico();
        ControladorMedico cm = new ControladorMedico(vistamedico, modelo);
    }

    private void cita() throws IOException {
        VistaCita vistacita = new VistaCita();
        ControladorCita cv = new ControladorCita(vistacita, modelo);
    }

   
}
