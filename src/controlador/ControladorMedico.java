
package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.IModelo;
import vista.VistaMedico;
import vista.VistaMenu;
import vista.VistaTerminal;
import modelo.Medico;
import vista.IVista;

/**
 * Fichero: ControladorMedico.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public class ControladorMedico {

    VistaTerminal vt = new VistaTerminal();
    VistaMenu menu = new VistaMenu();
    
    IModelo modelo;
    IVista<Medico> vistamedico;
    Medico medico;
    

    Controlador cont;

    public ControladorMedico(VistaMedico vistamedico, IModelo modelo) throws IOException {
        this.vistamedico = vistamedico;
        this.modelo = modelo;
        MenuMedico();
                
        
    }

    public char MenuMedico() throws IOException {
        boolean test = false;
        char opcion = ' ';
        //inicializaObjetos();
        do {          
            menu.MenuMedico();
            menu.MenuCrud();
            opcion = vt.leerLetra();

            switch (opcion) {
                case 'c': // Crea medico
                    create();
                    test = true;
                    break;
                case 'r':
                    read();
                    test = true;
                    break;
                case 'u':
                    update();
                    test = true;
                    break;
                case 'd':
                    delete();
                    test = true;
                    break;
                case 's':
                    vt.mostrarTexto("\nvolviendo al menu Estructura...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                }
               
        } while (test);
        return opcion;

    }

    private void create(){
        IVista<Medico> vm = new VistaMedico();                               
        Medico medico = vm.obtener();
        modelo.create(medico);
        
    }

    private void read(){
        VistaMedico vistamedico = new VistaMedico();
        vistamedico.mostrarMedicos(modelo.readm());

    }

    private void update(){
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Medico medico = null;
        Iterator iterator = modelo.readm().iterator();
        while (iterator.hasNext()) {
            Medico me = (Medico) iterator.next();
            if (id.equals(me.getId())) {
                medico = me;
            }
            if (medico != null){
                IVista<Medico> vp = new VistaMedico();
            medico = vp.obtener();
            medico.setId(id);
            modelo.update(medico);
            vt.mostrarTexto("Datos actualizados\n");
            break;
            } else {vt.mostrarTexto("El medico no existe\n");}
        }
    }

    private void delete(){
        
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Medico medico = null;
        Iterator iterator = modelo.readm().iterator();
        while (iterator.hasNext()) {
            Medico me = (Medico) iterator.next();
            if (id.equals(me.getId())) {
                medico = me;
            }
            if (medico != null) {
            modelo.delete(medico);
            vt.mostrarTexto("Medico borrado\n");
            break;
            } else {vt.mostrarTexto("El medico no existe\n");}
        }
     }
    public void inicializaObjetos() {

        Medico medico = new Medico();
                
        medico.setEdad(34);
        medico.setEspecialidad("ninguna");
        medico.setId("1");
        medico.setNcolegiado("53359063Y");
        medico.setNombre("Dario medico");
        medico.setObservaciones("ninguna");
        medico.setTelefono(646136523);
                
        modelo.create(medico);
    }
    
}
