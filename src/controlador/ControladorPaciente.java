package controlador;


import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.IModelo;
import vista.VistaMenu;
import vista.VistaPaciente;
import vista.VistaTerminal;
import modelo.Paciente;
import vista.IVista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class ControladorPaciente {

    VistaMenu menu = new VistaMenu();
    VistaTerminal vt = new VistaTerminal();
    
    IModelo modelo;
    IVista<Paciente> vistapaciente;
    Paciente paciente;
    
    

    //int opcion;
    //public int valor;

    ControladorPaciente(VistaPaciente vistapaciente, IModelo modelo) throws IOException {
        this.vistapaciente = vistapaciente;
        this.modelo = modelo;
        MenuPaciente();
                       
    }

    private char MenuPaciente() throws IOException {
        //inicializaObjetos();
        boolean test = false;
        char opcion = ' ';
       
        do {
            menu.MenuPaciente();
            menu.MenuCrud();
            opcion = vt.leerLetra();
           
            switch (opcion) {
                case 'c': // Crea paciente
                    create();
                    test = true;
                    break;
                case 'r':
                    read();
                    test = true;
                    break;
                case 'u':
                    update();
                    test = true;
                    break;
                case 'd':
                    delete();
                    test = true;
                    break;
                case 's':
                    vt.mostrarTexto("\nvolviendo al menu Estructura...\n");
                    test = false;
                    break;
                default: 
                    vt.mostrarTexto("\nIntroduce una opción del menú.\n");
                    break;
                }
               
        } while (test);
        return opcion;

    }

    private void create(){
        IVista<Paciente> vp = new VistaPaciente();                               
        Paciente paciente = vp.obtener();
        modelo.create(paciente);
        
    }

    private void read(){
        VistaPaciente vistapaciente = new VistaPaciente();
        vistapaciente.mostrarPacientes(modelo.readp());

    }

    private void update(){
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Paciente paciente = null;
        Iterator iterator = modelo.readp().iterator();
        while (iterator.hasNext()) {
            Paciente pa = (Paciente) iterator.next();
            if (id.equals(pa.getId())) {
                paciente = pa;
            }
            if (paciente != null){
                IVista<Paciente> vp = new VistaPaciente();
            paciente = vp.obtener();
            paciente.setId(id);
            modelo.update(paciente);
            vt.mostrarTexto("Datos actualizados\n");
            break;
            } else {vt.mostrarTexto("El paciente no existe\n");}
    }
    }

    private void delete(){
        
        vt.mostrarTexto("Ingrese el id a buscar\nId: ");
        String id = vt.pedirString();
        Paciente paciente = null;
        Iterator iterator = modelo.readp().iterator();
        while (iterator.hasNext()) {
            Paciente pa = (Paciente) iterator.next();
            if (id.equals(pa.getId())) {
                paciente = pa;
            }
            if (paciente != null) {
            modelo.delete(paciente);
            vt.mostrarTexto("Paciente borrado\n");
            } else {vt.mostrarTexto("El paciente no existe\n");}
        }
    }   
    
    public final void inicializaObjetos() {

       Paciente paciente = new Paciente();
       
       paciente.setId("1");
       paciente.setNss("53359063Y");
       paciente.setNombre("Dario");
       paciente.setEdad(34);
       paciente.setTelefono(646136523);
       paciente.setObservaciones("Estudiante");
        

        modelo.create(paciente);
    }
    
    public void buscarPaciente(String idpaciente){
        String nombrepaciente;
        Paciente paciente = null;
        boolean ep = true;
        
        do{
            Iterator iterator = modelo.readp().iterator();
            while (iterator.hasNext()) {
                 Paciente pa = (Paciente) iterator.next();
                if (idpaciente.equals(pa.getId())) {
                    nombrepaciente =pa.getNombre();
                    ep = false;
                } else {vt.mostrarTexto("Ningun paciente asociado\n");}
            }
        }while (ep);
        
    }
}