package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Fichero: ModeloHashset.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 16-dic-2015
 */
public class ModeloHashSet implements IModelo {
    
    private HashSet<Paciente> pacientes = new HashSet();
    private HashSet <Medico> medicos = new HashSet(); 
    private HashSet <Cita> citas = new HashSet();

   
@Override
    public void create(Paciente paciente) {
        pacientes.add(paciente);
    }

    @Override
    public void update(Paciente paciente) {
        
        Iterator it = pacientes.iterator();
        Paciente p;
        int pos = 0;

        while (it.hasNext()) {
            p = (Paciente) it.next();
                if (p.getId().equals(paciente.getId())) {
            pacientes.remove(p);
            // alumnos.add(alumno);
            // No se puede borrar y añadir en el mismo iterador
            }
        }
      pacientes.add(paciente);
    }

    @Override
    public void delete(Paciente paciente) {
        
        Iterator it = pacientes.iterator();
        Paciente a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Paciente) it.next();
            if (a.getId().equals(paciente.getId())) {
            pacientes.remove(a);
            break;
         }
      }
    
    }
    
     @Override
    public HashSet<Paciente> readp() {
        return pacientes;
    }

                        //******   MEDICO  ******//
    
    @Override
    public void create(Medico medico) {
        
        medicos.add(medico);
    }

    @Override
    public void update(Medico medico) {
       
        Iterator it = medicos.iterator();
        Medico m;
        int pos = 0;

        while (it.hasNext()) {
            m = (Medico) it.next();
                if (m.getId().equals(medico.getId())) {
            medicos.remove(m);
            // medicos.add(medico);
            // No se puede borrar y añadir en el mismo iterador
            }
        }
      medicos.add(medico);
    }

    @Override
    public void delete(Medico medico) {
        
        Iterator it = medicos.iterator();
        Medico m;
        int pos = 0;

        while (it.hasNext()) {
            m = (Medico) it.next();
            if (m.getId().equals(medico.getId())) {
            medicos.remove(m);
            break;
         }
      }
    
    }

    @Override
    public HashSet<Medico> readm() {
        return medicos;
        }

                        //******   CITA  ******//  
    
    @Override
    public void create(Cita cita) {
        
        citas.add(cita);
    
    }

    @Override
    public void update(Cita cita) {
        
        Iterator it = medicos.iterator();
        Cita c;
        int pos = 0;

        while (it.hasNext()) {
            c = (Cita) it.next();
                if (c.getId().equals(cita.getId())) {
            citas.remove(c);
            // medicos.add(medico);
            // No se puede borrar y añadir en el mismo iterador
            }
        }
      citas.add(cita);
    
    }

    @Override
    public void delete(Cita cita) {
       
        Iterator it = medicos.iterator();
        Cita c;
        int pos = 0;

        while (it.hasNext()) {
            c = (Cita) it.next();
            if (c.getId().equals(cita.getId())) {
            medicos.remove(c);
            break;
         }
      }
    
    }

    @Override
    public HashSet<Cita> readc() {
        return citas;
    
    }

}
