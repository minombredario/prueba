package modelo;

/**
 * Fichero: Cita.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Cita {

    private String id;
    private String dia;
    private String hora;
    private String paciente;
    private String medico;

    public Cita(){
        id = "";
        dia = "";
        hora = "";
        medico = "";
        paciente = "";
        
    }
    
    public Cita(String id, String dia, String hora, String medico, String paciente){
        this.id = id;
        this.dia = dia;
        this.hora = hora;
        this.paciente = paciente;
        this.medico = medico;
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public String toString() {
        return medico + "" + paciente + "\nId: " + id + "" + dia + "" + hora;
    }

}
