package modelo;

/**
 * Fichero: persona.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 14-dic-2015
 */
public class Persona {
     //private static final AtomicInteger count = new AtomicInteger(0);
    protected String id; // Permite acceder desde la subclase
    protected String nombre;
    protected int edad;
    protected int telefono;
    protected String observaciones;
    
   // private static int idsig = -4;
    

   public Persona(){
     //id = " " + idsig;
     id = "0";
     nombre = "dario";
     edad = 34;
     telefono = 646136523;
     observaciones = "estudiante";
     //idsig++;
   }

   public Persona (String id, String nombre, int edad, int telefono,
            String observaciones){
     this.id =id;
     this.nombre = nombre;
     this.edad = edad;
     this.telefono = telefono;
     this.observaciones = observaciones;
     
    }
   
  /* public static int getIdsig() {
        return idsig;
    }

    public static void setIdsig(int idsig) {
       Persona.idsig = idsig;
    }*/
   
    /**
     * @return the idpe
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the idpe to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String toString() {
        return id + nombre + edad + telefono +observaciones;
    }

}