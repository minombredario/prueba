/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usu1601
 */
public class ModeloFichero implements IModelo{
    int countp = 0;
    int countm = 0;
    int countc = 0;
    
    File filepacientes = new File("pacientes.csv");
    File filemedicos = new File("medicos.csv");
    File filecitas = new File("citas.csv");
    
    @Override
    public void create(Paciente paciente) {
        try {
            FileWriter filewriter = new FileWriter(filepacientes, true);
            filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
            filewriter.close();
            countp++;
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Paciente paciente) {
        File temp_pa = new File("temp.csv");
        Paciente pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Paciente(id, nombre, edad, telefono,
              observaciones, nss);
                
                if (paciente.getId().equals(id)) {
                    filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
                } else {
                    filewriter.write(
                      pa.getId()   + ";" + pa.getNombre() + ";"
                    + pa.getNss()  + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad() + ";" + pa.getTelefono() +";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public void delete(Paciente paciente) {
        File temp_pa = new File("temp.csv");
        Paciente pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader fr = new FileReader(filepacientes);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                String id = st.nextToken();
                String nombre = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Paciente(id, nombre, edad, telefono,
              observaciones, nss);
                if (!paciente.getId().equals(id)) {
                    filewriter.write(
                      pa.getId()   + ";" + pa.getNombre() + ";"
                    + pa.getNss()  + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad() + ";" + pa.getTelefono() +";\r\n");
                } 
                s = bf.readLine();
            }
            filewriter.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public HashSet<Paciente> readp() {
        
        HashSet hs = new HashSet();
        Paciente paciente = new Paciente();
        try {
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                paciente = new Paciente(id, nombre, edad, telefono,
              observaciones, nss);
                hs.add(paciente);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }

    @Override
    public void create(Medico medico) {
        try {
            FileWriter filewriter = new FileWriter(filemedicos, true);
            filewriter.write(
                      medico.getId()            + ";" + medico.getNombre()     + ";"
                    + medico.getObservaciones() + ";" + medico.getNcolegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
            filewriter.close();
            countm++;
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Medico medico) {
        File temp_me = new File("temp.csv");
        Medico doc;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                String id = st.nextToken();
                String nombre = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                                
                doc = new Medico(id, nombre, edad, telefono, observaciones, 
                        ncolegiado, especialidad);
                if (medico.getId().equals(id)) {
                    filewriter.write(
                      medico.getId()            + ";" + medico.getNombre()     + ";"
                    + medico.getObservaciones() + ";" + medico.getNcolegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
                    
                } else {
                    filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getObservaciones() + ";" + doc.getNcolegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad() + ";" + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public void delete(Medico medico) {
        File temp_me = new File("temp.csv");
        Medico doc;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader fr = new FileReader(filemedicos);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                String id = st.nextToken();
                String nombre = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                doc = new Medico(id, nombre,  edad, telefono,
                        ncolegiado, especialidad, observaciones);
                if (!medico.getId().equals(id)) {
                     filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getObservaciones() + ";" + doc.getNcolegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad() + ";" + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }
                s = bf.readLine();
            }
            filewriter.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public HashSet<Medico> readm() {
        HashSet hs = new HashSet();
        Medico medico = new Medico();
        try {
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                String observaciones = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                medico = new Medico(id, nombre,  edad, telefono, observaciones,
                        ncolegiado, especialidad );
                hs.add(medico);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }

    @Override
    public void create(Cita cita) {
        try {
            FileWriter filewriter = new FileWriter(filecitas, true);
            filewriter.write(
                      cita.getId()       + ";" + cita.getDia()    + ";" 
                    + cita.getHora()     + ";" + cita.getMedico() + ";" 
                    + cita.getPaciente() + ";\r\n");
            filewriter.close();
            countc++;
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Cita cita) {
        File temp_ci = new File("temp.csv");
        Cita ci;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                String id = st.nextToken();
                String dia = st.nextToken();
                String hora = st.nextToken();
                String medico = st.nextToken();
                String paciente = st.nextToken();
                                
                ci = new Cita(id, dia, hora, medico, paciente);
                if (cita.getId().equals(id)) {
                    filewriter.write(
                      cita.getId()       + ";" + cita.getDia()    + ";" 
                    + cita.getHora()     + ";" + cita.getMedico() + ";" 
                    + cita.getPaciente() + ";\r\n");
                    
                } else {
                    filewriter.write(
                      ci.getId()       + ";" + ci.getDia()    + ";" 
                    + ci.getHora()     + ";" + ci.getMedico() + ";" 
                    + ci.getPaciente() + ";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public void delete(Cita cita) {
        File temp_ci = new File("temp.csv");
        Cita ci;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader fr = new FileReader(filecitas);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                String id = st.nextToken();
                String dia = st.nextToken();
                String hora = st.nextToken();
                String medico = st.nextToken();
                String paciente = st.nextToken();
                                
                ci = new Cita(id, dia, hora, medico, paciente);
                if (cita.getId().equals(id)) {
                    filewriter.write(
                      cita.getId()       + ";" + cita.getDia()    + ";" 
                    + cita.getHora()     + ";" + cita.getMedico() + ";" 
                    + cita.getPaciente() + ";\r\n");
               
                }
                s = bf.readLine();
            }
            filewriter.close();
            fr.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public HashSet<Cita> readc() {
        HashSet hs = new HashSet();
        Cita cita = new Cita();
        try {
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                String id = st.nextToken();
                String dia = st.nextToken();
                String hora = st.nextToken();
                String medico = st.nextToken();
                String paciente = st.nextToken();
                                                    
                cita = new Cita(id, dia, hora, medico, paciente);
                hs.add(cita);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }
    
}
