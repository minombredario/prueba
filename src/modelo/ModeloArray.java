package modelo;


import java.util.HashSet;
import vista.VistaTerminal;



/**
 * Fichero: VistaArrayList.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 13-dic-2015
 */
public class ModeloArray implements IModelo {
    
    Paciente pacientes[] = new Paciente[100];
    Medico medicos[] = new Medico[100];
    Cita citas[] = new Cita[100];
    
    VistaTerminal vt;
    
    int countp = 0;
    int countm = 0;
    int countc = 0;

    public ModeloArray() {

        //inicializamos cada Array para poder recorrerlos//
        for (int i = 0; i < pacientes.length; i++) {
            pacientes[i] = new Paciente("", "", 0, 0, "", "");
        }

        for (int i = 0; i < medicos.length; i++) {
            medicos[i] = new Medico("", "", 0, 0, "", "", "");
        }

        for (int i = 0; i < citas.length; i++) {
            citas[i] = new Cita("", "", "", "", "");
        }
    }

                        //******   PACIENTE  ******//
    @Override
    public void create(Paciente paciente) {
        
       pacientes[countp]= paciente;    
       countp++;
         
        }

    @Override
    public void update(Paciente paciente) {
       
        for (int i = 0; i < pacientes.length; i++) {
            if (pacientes[i].getId().equals(paciente.getId())) {
                pacientes[i] = paciente;
            }
        }   
    }
    
    @Override
    public void delete(Paciente paciente) {
        
        for (int i = 0; i < pacientes.length; i++) {
            if (pacientes[i].getId().equals(paciente.getId())) {
                pacientes[i] = new Paciente ("", "", 0, 0, "", "");
            }
        }
    }        
    
     @Override
    public HashSet<Paciente> readp() {
        
        HashSet hashset = new HashSet();
        for (Paciente paciente : pacientes) {
            if (!paciente.getId().equals("")) {
                hashset.add(paciente);
            }
        }
        return hashset;
       
    }
    
                        //******   MEDICO  ******//
    
    @Override
    public void create(Medico medico) {
        
        medicos[countm]= medico;    
        countm++;
    }

    @Override
    public void update(Medico medico) {
     
       for (int i = 0; i < medicos.length; i++) {
            if (medicos[i].getId().equals(medico.getId())) {
                medicos[i] = medico;
            }
        }   
    }

    @Override
    public void delete(Medico medico) {
        
        for (int i = 0; i < medicos.length; i++) {
            if (medicos[i].getId().equals(medico.getId())) {
                medicos[i] = new Medico("", "", 0, 0, "", "", "");
            }
        }
    }

    @Override
    public HashSet<Medico> readm() {
        
        HashSet hashset = new HashSet();
        for (int i = 0; i < countm; i++) {
            if ( !medicos[i].getId().equals("")) {
                hashset.add(medicos[i]);
            }
        }
        return hashset;
        }

                        //******   CITA  ******//  
    
    @Override
    public void create(Cita cita) {
        
        citas[countc]= cita;    
        countc++;
    }

    @Override
    public void update(Cita cita) {
    
        for (int i = 0; i < citas.length; i++) {
            if (citas[i].getId().equals(cita.getId())) {
                citas[i] = cita;
            }
        }   
    }

    @Override
    public void delete(Cita cita) {
        
        for (int i = 0; i < citas.length; i++) {
            if (citas[i].getId().equals(cita.getId())) {
                citas[i] = new Cita("", "", "", "", "");
            }
        }
    }

    @Override
    public HashSet<Cita> readc() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < countc; i++) {
            if ( !citas[i].getId().equals("")) {
                hashset.add(citas[i]);
            }
        }
        return hashset;
    
    }

   
}
